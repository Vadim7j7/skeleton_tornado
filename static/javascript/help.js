/*
*
* Created: Vadim7j7
* Date: 07.02.2015
*
*/

(function() {
    /* Variables */
    var HELP, _this;
    /* ... */

    /* Initialized */
    HELP = function () {};
    _this = this;
    /* ... */

    /* Find elements by class name */
    HELP.prototype.getElsByClassName = function(name, node) {
        var result = null;
        node = node ? node : _this.document;

        if (node.getElementsByClassName) {
            result = node.getElementsByClassName(name);
        } else {
            for (var r = [], e = node.getElementsByTagName('*'), i = e.length; i--;) {
                if ((' ' + e[i].className + ' ').indexOf(' '+ name +' ') > -1) {
                    r.push(e[i]);
                }
            }
            result = r;
        }
        return result;
    };
    /* end */

    /* Create class event crossbrowser */
    var EVENT = function() {
        this.guid = 0;
        function fixEvent(event) {
            event = event || _this.event;
            if (event.isFixed) {
                return event;
            }
            event.isFixed = true;
            event.preventDefault = event.preventDefault || function(){this.returnValue = false;};
            event.stopPropagation = event.stopPropagaton || function(){this.cancelBubble = true;};
            if (!event.target) {
                event.target = event.srcElement;
            }
            if (!event.relatedTarget && event.fromElement) {
                event.relatedTarget = event.fromElement === event.target ? event.toElement : event.fromElement;
            }
            if (event.pageX === null && event.clientX !== null) {
                var html = _this.document.documentElement, body = _this.document.body;
                event.pageX = event.clientX + (html && html.scrollLeft || body && body.scrollLeft || 0) - (html.clientLeft || 0);
                event.pageY = event.clientY + (html && html.scrollTop || body && body.scrollTop || 0) - (html.clientTop || 0);
            }
            if (!event.which && event.button) {
                event.which = (event.button & 1 ? 1 : ( event.button & 2 ? 3 : ( event.button & 4 ? 2 : 0 ) ));
            }

            return event;
        };
        this.commonHandler = function(event){
            event = fixEvent(event);
            var handlers = this.events[event.type];
            for ( var g in handlers ) {

                var ret = handlers[g].call(this, event);
                if ( ret === false ) {
                    event.preventDefault();
                    event.stopPropagation();
                }
            }
        };
    };
    EVENT.prototype.add = function(elem, type, handler) {
        if (elem == undefined || elem == null) {
            return null;
        }

        if (elem.setInterval && ( elem !== _this && !elem.frameElement )) {
            elem = _this;
        }

        if (!handler.guid) {
            var iii = ++this.guid
            handler.guid = iii;
            elem.guid = iii;
        }

        if (!elem.events) {
            elem.events = {};
            var ch = this.commonHandler;
            elem.handle = function(event) {
                if (typeof EVENT !== "undefined") {
                    return ch.call(elem, event);
                }
            };
        }

        if (!elem.events[type]) {
            elem.events[type] = {};
            if (elem.addEventListener) {
                elem.addEventListener(type, elem.handle, false);
            } else if (elem.attachEvent) {
                elem.attachEvent("on" + type, elem.handle);
            }
        }

        elem.events[type][handler.guid] = handler;
    };
    EVENT.prototype.remove = function(elem, type, handler) {
        var handlers = elem.events && elem.events[type];
        if (!handlers) return;

        for (var han in handlers) {
            delete handlers[han];
            break;
        }

        for(var any in handlers) return;
        if (elem.removeEventListener) {
            elem.removeEventListener(type, elem.handle, false);
        } else {
            elem.detachEvent("on" + type, elem.handle);
        }

        delete elem.events[type];

        for (var any in elem.events) return;
        try {
            delete elem.handle;
            delete elem.events;
        } catch(e) {
            elem.removeAttribute("handle");
            elem.removeAttribute("events");
        }
    };
    HELP.prototype.event = new EVENT();
    /* end */

    /* ... */
    _this.help = new HELP();
    /* ... */
}).call(window);