# ----------------------------------------------------------------------------------------------------------------------
from os.path import join
from os import getcwd


# ----------------------------------------------------------------------------------------------------------------------
DIR = getcwd()
STATIC_PATCH = join(DIR, 'static')
TEMPLATE_PATH = join(DIR, 'templates')

MONGODB = {'user': 'user',
           'password': 'psw',
           'db': 'db',
           'host': '192.168.0.10',
           'port': '27017'}


def mongodb_dsn():
    return 'mongodb://%s:%s@%s:%s/%s' % (MONGODB['user'], MONGODB['password'], MONGODB['host'], MONGODB['port'],
                                         MONGODB['db'])


# ----------------------------------------------------------------------------------------------------------------------
