# --- coding: utf8 ---
__author__ = 'vadim'
# ----------------------------------------------------------------------------------------------------------------------
import tornado.httpserver
import tornado.ioloop
import tornado.web
from tornado.options import define, options
from motor import MotorClient
from jinja2 import Environment, FileSystemLoader
from setting import TEMPLATE_PATH, STATIC_PATCH, mongodb_dsn
from setting.routs import rout_all

# ----------------------------------------------------------------------------------------------------------------------
define("port", default=8001, help="run on the given port", type=int)
define('debug', default=True, help="env type", type=bool)


# ----------------------------------------------------------------------------------------------------------------------
class Application(tornado.web.Application):
    def __init__(self):
        setting = {
            'static_path': STATIC_PATCH,
            'template_path': TEMPLATE_PATH,
            'debug': options.debug,
            'xsrf_cookies': True,
            'cookie_secret': '61oETzKXQAGasdkf5sEmGeJJFuYh7EQnp2Xd8TP27o/Vo='}
        tornado.web.Application.__init__(self, rout_all, **setting)
        self.db = MotorClient(mongodb_dsn())
        self.env = Environment(loader=FileSystemLoader(TEMPLATE_PATH))


# ----------------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    try:
        options.parse_command_line()
        http_server = tornado.httpserver.HTTPServer(Application())
        http_server.listen(options.port, address='127.0.0.1')
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        print ('Exit')


# ----------------------------------------------------------------------------------------------------------------------
